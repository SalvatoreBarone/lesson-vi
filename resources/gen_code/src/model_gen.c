/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model_gen.c
 *
 * Code generated for Simulink model 'model_gen'.
 *
 * Model version                  : 1.28
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Wed Jan 15 14:57:43 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "model_gen.h"
#include "model_gen_private.h"

/* Named constants for Chart: '<Root>/Semaphore ' */
#define model_ge_IN_avviso_via_impedita ((uint8_T)1U)
#define model_gen_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define model_gen_IN_init              ((uint8_T)2U)
#define model_gen_IN_limit_30          ((uint8_T)3U)
#define model_gen_IN_no_limit          ((uint8_T)4U)
#define model_gen_IN_via_impedita      ((uint8_T)5U)

/* Model step function */
void model_gen_step(RT_MODEL_model_gen_T *const model_gen_M, boolean_T
                    model_gen_U_In1, boolean_T model_gen_U_In2, uint8_T
                    model_gen_U_In3, uint8_T *model_gen_Y_proj_1, uint8_T
                    *model_gen_Y_proj_3, uint8_T *model_gen_Y_proj_2)
{
  DW_model_gen_T *model_gen_DW = ((DW_model_gen_T *) model_gen_M->dwork);

  /* Chart: '<Root>/Semaphore ' incorporates:
   *  Inport: '<Root>/In1'
   *  Inport: '<Root>/In2'
   *  Inport: '<Root>/In3'
   */
  if (model_gen_DW->is_active_c3_model_gen == 0U) {
    model_gen_DW->is_active_c3_model_gen = 1U;
    model_gen_DW->is_c3_model_gen = model_gen_IN_init;
  } else {
    switch (model_gen_DW->is_c3_model_gen) {
     case model_ge_IN_avviso_via_impedita:
      if (model_gen_U_In1) {
        model_gen_DW->is_c3_model_gen = model_gen_IN_via_impedita;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 1U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      } else if (model_gen_U_In2 && (model_gen_U_In3 == 0)) {
        model_gen_DW->is_c3_model_gen = model_gen_IN_no_limit;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 3U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      } else {
        if (model_gen_U_In2 && (model_gen_U_In3 == 1)) {
          model_gen_DW->is_c3_model_gen = model_gen_IN_limit_30;

          /* Outport: '<Root>/Out1' */
          *model_gen_Y_proj_1 = 2U;

          /* Outport: '<Root>/Out3' */
          *model_gen_Y_proj_2 = 3U;

          /* Outport: '<Root>/Out2' */
          *model_gen_Y_proj_3 = 0U;
        }
      }
      break;

     case model_gen_IN_init:
      if (!model_gen_U_In1) {
        model_gen_DW->is_c3_model_gen = model_gen_IN_no_limit;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 3U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      } else {
        model_gen_DW->is_c3_model_gen = model_gen_IN_via_impedita;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 1U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      }
      break;

     case model_gen_IN_limit_30:
      if (model_gen_U_In1) {
        model_gen_DW->is_c3_model_gen = model_gen_IN_via_impedita;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 1U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      }
      break;

     case model_gen_IN_no_limit:
      if (model_gen_U_In1) {
        model_gen_DW->is_c3_model_gen = model_gen_IN_via_impedita;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 1U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      }
      break;

     default:
      /* case IN_via_impedita: */
      if (!model_gen_U_In1) {
        model_gen_DW->is_c3_model_gen = model_ge_IN_avviso_via_impedita;

        /* Outport: '<Root>/Out1' */
        *model_gen_Y_proj_1 = 2U;

        /* Outport: '<Root>/Out3' */
        *model_gen_Y_proj_2 = 0U;

        /* Outport: '<Root>/Out2' */
        *model_gen_Y_proj_3 = 0U;
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Semaphore ' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  model_gen_M->Timing.t[0] =
    (++model_gen_M->Timing.clockTick0) * model_gen_M->Timing.stepSize0;

  {
    /* Update absolute timer for sample time: [0.2s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.2, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     */
    model_gen_M->Timing.clockTick1++;
  }
}

/* Model initialize function */
void model_gen_initialize(RT_MODEL_model_gen_T *const model_gen_M, boolean_T
  *model_gen_U_In1, boolean_T *model_gen_U_In2, uint8_T *model_gen_U_In3,
  uint8_T *model_gen_Y_proj_1, uint8_T *model_gen_Y_proj_3, uint8_T
  *model_gen_Y_proj_2)
{
  DW_model_gen_T *model_gen_DW = ((DW_model_gen_T *) model_gen_M->dwork);

  /* Registration code */
  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&model_gen_M->solverInfo,
                          &model_gen_M->Timing.simTimeStep);
    rtsiSetTPtr(&model_gen_M->solverInfo, &rtmGetTPtr(model_gen_M));
    rtsiSetStepSizePtr(&model_gen_M->solverInfo, &model_gen_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&model_gen_M->solverInfo, ((const char_T **)
      (&rtmGetErrorStatus(model_gen_M))));
    rtsiSetRTModelPtr(&model_gen_M->solverInfo, model_gen_M);
  }

  rtsiSetSimTimeStep(&model_gen_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&model_gen_M->solverInfo,"FixedStepDiscrete");
  rtmSetTPtr(model_gen_M, &model_gen_M->Timing.tArray[0]);
  model_gen_M->Timing.stepSize0 = 0.2;

  /* states (dwork) */
  (void) memset((void *)model_gen_DW, 0,
                sizeof(DW_model_gen_T));

  /* external inputs */
  *model_gen_U_In1 = false;
  *model_gen_U_In2 = false;
  *model_gen_U_In3 = 0U;

  /* external outputs */
  (*model_gen_Y_proj_1) = 0U;
  (*model_gen_Y_proj_3) = 0U;
  (*model_gen_Y_proj_2) = 0U;

  /* SystemInitialize for Chart: '<Root>/Semaphore ' */
  model_gen_DW->is_active_c3_model_gen = 0U;
  model_gen_DW->is_c3_model_gen = model_gen_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void model_gen_terminate(RT_MODEL_model_gen_T *const model_gen_M)
{
  /* (no terminate code required) */
  UNUSED_PARAMETER(model_gen_M);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
