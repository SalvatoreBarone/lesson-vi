#include "drive_signal.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#define LED_GREEN	LED4
#define LED_YELLOW	LED3
#define LED_RED		LED5

#define model_ge_IN_avviso_via_impedita ((uint8_t)1U)
#define model_gen_IN_NO_ACTIVE_CHILD   ((uint8_t)0U)
#define model_gen_IN_init              ((uint8_t)2U)
#define model_gen_IN_limit_30          ((uint8_t)3U)
#define model_gen_IN_no_limit          ((uint8_t)4U)
#define model_gen_IN_via_impedita      ((uint8_t)5U)

void drive_signal(RT_MODEL_model_gen_T * const model_gen_M)
{
	DW_model_gen_T *model_gen_DW = ((DW_model_gen_T *) model_gen_M->dwork);

	switch(model_gen_DW->is_c3_model_gen)
	{
		case model_ge_IN_avviso_via_impedita:
			BSP_LED_On(LED_YELLOW);
			BSP_LED_Off(LED_GREEN);
			BSP_LED_Off(LED_RED);
			break;

		case model_gen_IN_limit_30:
			BSP_LED_On(LED_YELLOW);
			BSP_LED_On(LED_GREEN);
			BSP_LED_Off(LED_RED);
			break;

		case model_gen_IN_no_limit:
			BSP_LED_Off(LED_YELLOW);
			BSP_LED_On(LED_GREEN);
			BSP_LED_Off(LED_RED);
			break;

		default:
		//case model_gen_IN_via_impedita:
			BSP_LED_Off(LED_YELLOW);
			BSP_LED_Off(LED_GREEN);
			BSP_LED_On(LED_RED);
			break;

	}
}
