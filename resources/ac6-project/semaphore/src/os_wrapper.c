#include "os_wrapper.h"

int32_t OS_GetTaskId(void)
{
    TaskStatus_t task_status;
    vTaskGetInfo(NULL, &task_status, pdFALSE, eRunning);
    return task_status.xTaskNumber;
}

