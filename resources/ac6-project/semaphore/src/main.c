/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "model_gen.h"
#include "rtwtypes.h"
#include "rand.h"
#include "drive_signal.h"

#include "os_wrapper.h"

uint8_t get_bacc_status(void);

void pdt_sensor_task(void*);
void bacc_sensor_task(void*);
void semaphore_task(void*);

RT_MODEL_model_gen_T model_gen_M_;

QueueHandle_t pdt_sem;
QueueHandle_t bacc_sem;

int main(void)
{
	HAL_Init();
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);

	OS_Queue_create(pdt_sem, 10, sizeof(uint8_t));
	OS_Queue_create(pdt_sem, 3, sizeof(uint8_t));

	OS_Task_create(bacc_sensor_task, OS_TASK_MIN_STACK_SIZE, NULL, OS_TASK_MAX_PRIORITY);
	OS_Task_create(pdt_sensor_task, OS_TASK_MIN_STACK_SIZE, NULL, OS_TASK_MAX_PRIORITY);
	OS_Task_create(semaphore_task, OS_TASK_MIN_STACK_SIZE, NULL, OS_TASK_MAX_PRIORITY-2);

	OS_Start_Scheduler();

	for(;;);
}

uint8_t get_bacc_status(void)
{
	uint32_t bacc_status_1 = BSP_PB_GetState(BUTTON_KEY);
	HAL_Delay(100);
	uint32_t bacc_status_2 = BSP_PB_GetState(BUTTON_KEY);
	if (bacc_status_1 == 1 && bacc_status_1 == bacc_status_2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void pdt_sensor_task(void* args)
{
	for (;;)
	{
		uint8_t message_received = (rand() % 2 == 0 ? 0 : 1);
		uint8_t message_payload = (rand() % 2 == 0 ? 0 : 1);

		if (message_received == 1)
		{
			OS_Queue_nonblock_send(pdt_sem, message_payload);
		}

		OS_Task_Wait(100);
	}
}

void bacc_sensor_task(void* args)
{
	for (;;)
	{
		uint8_t sensor_data = get_bacc_status();

		if (sensor_data == 1)
		{
			OS_Queue_nonblock_send(bacc_sem, sensor_data);
		}

		OS_Task_Wait(100);
	}
}

void semaphore_task(void* args)
{
	RT_MODEL_model_gen_T * model_gen_MPtr = &model_gen_M_;
	DW_model_gen_T model_gen_DW;
	boolean_T model_gen_U_In1; 	/* BCAA sensor */
	boolean_T model_gen_U_In2; 	/* message from PDT received */
	uint8_T model_gen_U_In3;		/* kind of message received from PDT */
	uint8_T model_gen_Y_proj_1;
	uint8_T model_gen_Y_proj_3;
	uint8_T model_gen_Y_proj_2;

	/* Pack model data into RTM */
	model_gen_MPtr->dwork = &model_gen_DW;

	model_gen_U_In1 = get_bacc_status();
		model_gen_U_In2 = 0;
		model_gen_U_In3 = 0;

		/* Initialize model */
		model_gen_initialize(
				model_gen_MPtr,
				&model_gen_U_In1,
				&model_gen_U_In2,
				&model_gen_U_In3,
				&model_gen_Y_proj_1,
				&model_gen_Y_proj_3,
				&model_gen_Y_proj_2);

		/* Model Step */
		model_gen_step(
				model_gen_MPtr,
				model_gen_U_In1,
				model_gen_U_In2,
				model_gen_U_In3,
				&model_gen_Y_proj_1,
				&model_gen_Y_proj_3,
				&model_gen_Y_proj_2);

		drive_signal(model_gen_MPtr);

	for(;;)
	{
		OS_Queue_nonblock_recv(bacc_sem, &model_gen_U_In1);

		if (SUCCESS == OS_Queue_nonblock_recv(pdt_sem, &model_gen_U_In3))
			model_gen_U_In2 = 1;
		else
			model_gen_U_In2 = 0;

		model_gen_step(
			model_gen_MPtr,
			model_gen_U_In1,
			model_gen_U_In2,
			model_gen_U_In3,
			&model_gen_Y_proj_1,
			&model_gen_Y_proj_3,
			&model_gen_Y_proj_2);

		drive_signal(model_gen_MPtr);

		OS_Task_Wait(100);
	}
}

