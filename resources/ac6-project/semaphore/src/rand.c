#include "rand.h"

static uint16_t lfsr = 0xACE1u;
static uint32_t bit;

uint32_t rand(void)
{
    bit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
    return lfsr =  (lfsr >> 1) | (bit << 15);
}
