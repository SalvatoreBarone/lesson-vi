#ifndef OS_WRAPPER_H
#define OS_WRAPPER_H

#include <inttypes.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"

#define OS_SUCCESS pdTRUE
#define OS_FAILURE pdFALSE

//*****************************************************************************
// Queues

typedef QueueHandle_t OS_Queue_t;
typedef QueueHandle_t* OS_Queue_Ptr_t;

#define OS_Queue_create(queue, elements, size) \
	((queue = xQueueCreate(elements, size)) == NULL ? OS_FAILURE : OS_SUCCESS)

#define OS_Queue_block_send(queue, data) \
	xQueueSend(queue, data, portMAX_DELAY)

#define OS_Queue_block_recv(queue, data) \
	xQueueReceive(queue, data, portMAX_DELAY)

#define OS_Queue_nonblock_send(queue, data) \
	xQueueSend(queue, data, 0)

#define OS_Queue_nonblock_recv(queue,data) \
	xQueueReceive(queue,	data, 0)

#define OS_Queue_send_fromISR(queue, data)	\
	(pdTRUE == xQueueSendFromISR(queue, data, NULL) ? OS_SUCCESS : OS_FAILURE)

#define OS_Queue_recv_fromISR(queue, data) \
	(pdTRUE == xQueueReceiveFromISR(queue, data, NULL) ? OS_SUCCESS : OS_FAILURE)

//*****************************************************************************
// Semaphores

typedef SemaphoreHandle_t OS_Semaphore_t;

#define OS_Mutex_create(sem) \
	((sem = xSemaphoreCreateMutex()) != NULL ? OS_SUCCESS : OS_FAILURE)

#define OS_Semaphore_lock(sem) \
	(pdPASS != xSemaphoreTake(sem, portMAX_DELAY) ? OS_FAILURE : OS_SUCCESS)

#define OS_Semaphore_unlock(sem) \
	(pdPASS != xSemaphoreGive(sem) ? OS_FAILURE : OS_SUCCESS)


//*****************************************************************************
// Tasks

#define OS_TASK_MAX_PRIORITY	(configMAX_PRIORITIES-1)
#define OS_TASK_MIN_PRIORITY	tskIDLE_PRIORITY

#define OS_STACK_SIZE_T			uint32_t
#define OS_TASK_MIN_STACK_SIZE	configMINIMAL_STACK_SIZE

#define OS_DECLARE_TASK(task_name, task_args_ptr) \
	void task_name (void* task_args_ptr)

#define OS_Task_create(starting_routine, stack_size, args, priority) \
	xTaskCreate(\
			starting_routine,\
			#starting_routine,\
		   	stack_size,\
		   	args,\
		   	priority,\
		   	NULL)

#define OS_Start_Scheduler()	vTaskStartScheduler()

int32_t OS_GetTaskId(void);

#define OS_getTickCount()	xTaskGetTickCount()

#define OS_Task_Wait(period_ms) \
	TickType_t current_timer = xTaskGetTickCount();\
	vTaskDelayUntil(&current_timer, pdMS_TO_TICKS(period_ms));


#endif
