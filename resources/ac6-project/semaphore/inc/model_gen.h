/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model_gen.h
 *
 * Code generated for Simulink model 'model_gen'.
 *
 * Model version                  : 1.28
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Wed Jan 15 14:57:43 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_model_gen_h_
#define RTW_HEADER_model_gen_h_
#include <string.h>
#ifndef model_gen_COMMON_INCLUDES_
# define model_gen_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* model_gen_COMMON_INCLUDES_ */

#include "model_gen_types.h"
#include "rt_defines.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  uint8_T is_active_c3_model_gen;      /* '<Root>/Semaphore ' */
  uint8_T is_c3_model_gen;             /* '<Root>/Semaphore ' */
} DW_model_gen_T;

/* Real-time Model Data Structure */
struct tag_RTM_model_gen_T {
  const char_T * volatile errorStatus;
  DW_model_gen_T *dwork;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Model entry point functions */
extern void model_gen_initialize(RT_MODEL_model_gen_T *const model_gen_M,
  boolean_T *model_gen_U_In1, boolean_T *model_gen_U_In2, uint8_T
  *model_gen_U_In3, uint8_T *model_gen_Y_proj_1, uint8_T *model_gen_Y_proj_3,
  uint8_T *model_gen_Y_proj_2);
extern void model_gen_step(RT_MODEL_model_gen_T *const model_gen_M, boolean_T
  model_gen_U_In1, boolean_T model_gen_U_In2, uint8_T model_gen_U_In3, uint8_T
  *model_gen_Y_proj_1, uint8_T *model_gen_Y_proj_3, uint8_T *model_gen_Y_proj_2);
extern void model_gen_terminate(RT_MODEL_model_gen_T *const model_gen_M);

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Step' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'model_gen'
 * '<S1>'   : 'model_gen/Semaphore '
 */
#endif                                 /* RTW_HEADER_model_gen_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
