/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/
#include <stddef.h>
#include <stdio.h>


#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "model_gen.h"
#include "rtwtypes.h"
#include "rand.h"
#include "drive_signal.h"

static RT_MODEL_model_gen_T model_gen_M_;
static RT_MODEL_model_gen_T *const model_gen_MPtr = &model_gen_M_;
static DW_model_gen_T model_gen_DW;
static boolean_T model_gen_U_In1; 	/* BCAA sensor */
static boolean_T model_gen_U_In2; 	/* message from PDT received */
static uint8_T model_gen_U_In3;		/* kind of message received from PDT */
static uint8_T model_gen_Y_proj_1;
static uint8_T model_gen_Y_proj_3;
static uint8_T model_gen_Y_proj_2;

uint8_t get_bacc_status(void);




int main(void)
{
	HAL_Init();
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);

	RT_MODEL_model_gen_T *const model_gen_M = model_gen_MPtr;

	/* Pack model data into RTM */
	model_gen_M->dwork = &model_gen_DW;

	model_gen_U_In1 = get_bacc_status();
	model_gen_U_In2 = 0;
	model_gen_U_In3 = 0;

	/* Initialize model */
	model_gen_initialize(model_gen_M, &model_gen_U_In1, &model_gen_U_In2,
		                       &model_gen_U_In3, &model_gen_Y_proj_1,
		                       &model_gen_Y_proj_3, &model_gen_Y_proj_2);

	/* Model Step */
			model_gen_step(
					model_gen_M,
					model_gen_U_In1,
					model_gen_U_In2,
					model_gen_U_In3,
					&model_gen_Y_proj_1,
					&model_gen_Y_proj_3,
					&model_gen_Y_proj_2);

	drive_signal(model_gen_M);

	for (;;)
	{

		model_gen_U_In1 = get_bacc_status();
		/*
		 * Message from PDT is only simulated...
		 */
		model_gen_U_In2 = (rand() % 2 == 0 ? 0 : 1);
		/*
		 * the kind of message is also simulated
		 */
		model_gen_U_In3 = (rand() % 2 == 0 ? 0 : 1);


		/* Model Step */
		model_gen_step(
				model_gen_M,
				model_gen_U_In1,
				model_gen_U_In2,
				model_gen_U_In3,
				&model_gen_Y_proj_1,
				&model_gen_Y_proj_3,
				&model_gen_Y_proj_2);

		drive_signal(model_gen_M);

	}



	model_gen_terminate(model_gen_M);
	for(;;);


}

uint8_t get_bacc_status(void)
{
	/*
	 * Button is used as BACC sensor.
	 * The program performing debouncing operations before setting BACC status
	 */
	uint32_t bacc_status_1 = BSP_PB_GetState(BUTTON_KEY);
	HAL_Delay(100);
	uint32_t bacc_status_2 = BSP_PB_GetState(BUTTON_KEY);
	if (bacc_status_1 == 1 && bacc_status_1 == bacc_status_2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
