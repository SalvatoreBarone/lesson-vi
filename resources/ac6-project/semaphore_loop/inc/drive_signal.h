#ifndef DRIVE_SIGNAL_H
#define DRIVE_SIGNAL_H

#include "model_gen.h"

void drive_signal(RT_MODEL_model_gen_T * const model_gen_M);

#endif
