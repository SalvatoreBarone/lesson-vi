#ifndef MYRAND_H
#define MYRAND_H

#include <inttypes.h>

uint32_t rand(void);

#endif
