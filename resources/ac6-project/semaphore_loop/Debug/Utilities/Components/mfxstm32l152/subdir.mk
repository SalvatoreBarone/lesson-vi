################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Utilities/Components/mfxstm32l152/mfxstm32l152.c 

OBJS += \
./Utilities/Components/mfxstm32l152/mfxstm32l152.o 

C_DEPS += \
./Utilities/Components/mfxstm32l152/mfxstm32l152.d 


# Each subdirectory must supply rules for building sources it contributes
Utilities/Components/mfxstm32l152/%.o: ../Utilities/Components/mfxstm32l152/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F407VGTx -DSTM32F407G_DISC1 -DDEBUG -DSTM32F407xx -DUSE_HAL_DRIVER -I"/home/ssaa/workspace/provva_2/Utilities/Components/ili9325" -I"/home/ssaa/workspace/provva_2/Utilities/Components/s25fl512s" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ili9341" -I"/home/ssaa/workspace/provva_2/Utilities/Components/cs43l22" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ampire480272" -I"/home/ssaa/workspace/provva_2/Utilities/Components/n25q512a" -I"/home/ssaa/workspace/provva_2/Utilities/Components/s5k5cag" -I"/home/ssaa/workspace/provva_2/Utilities/Components/mfxstm32l152" -I"/home/ssaa/workspace/provva_2/CMSIS/device" -I"/home/ssaa/workspace/provva_2/Utilities/Components/n25q128a" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ts3510" -I"/home/ssaa/workspace/provva_2/Utilities/Components/st7735" -I"/home/ssaa/workspace/provva_2/HAL_Driver/Inc/Legacy" -I"/home/ssaa/workspace/provva_2/Utilities/Components/lis302dl" -I"/home/ssaa/workspace/provva_2/Utilities/Components/otm8009a" -I"/home/ssaa/workspace/provva_2/Utilities/Components/stmpe1600" -I"/home/ssaa/workspace/provva_2/Utilities/Components/Common" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ov2640" -I"/home/ssaa/workspace/provva_2/Utilities/Components/l3gd20" -I"/home/ssaa/workspace/provva_2/HAL_Driver/Inc" -I"/home/ssaa/workspace/provva_2/Utilities" -I"/home/ssaa/workspace/provva_2/Utilities/Components/stmpe811" -I"/home/ssaa/workspace/provva_2/Utilities/Components/lis3dsh" -I"/home/ssaa/workspace/provva_2/Utilities/Components/wm8994" -I"/home/ssaa/workspace/provva_2/Utilities/Components/n25q256a" -I"/home/ssaa/workspace/provva_2/inc" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ls016b8uy" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ft6x06" -I"/home/ssaa/workspace/provva_2/Utilities/STM32F4-Discovery" -I"/home/ssaa/workspace/provva_2/Utilities/Components/exc7200" -I"/home/ssaa/workspace/provva_2/Utilities/Components/st7789h2" -I"/home/ssaa/workspace/provva_2/Utilities/Components/ampire640480" -I"/home/ssaa/workspace/provva_2/Utilities/Components/lsm303dlhc" -I"/home/ssaa/workspace/provva_2/CMSIS/core" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


