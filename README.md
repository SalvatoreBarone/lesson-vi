[![pipeline status](https://gitlab.com/SalvatoreBarone/lesson-vi/badges/master/pipeline.svg)](https://gitlab.com/SalvatoreBarone/lesson-vi/commits/master) 
[![Download](https://img.shields.io/badge/lesson-vi.pdf-green.svg)](https://gitlab.com/SalvatoreBarone/lesson-vi/-/jobs/artifacts/master/raw/lesson-vi.pdf?job=expose)


# Safety Critical Systems for Railway Traffic Management

# Lesson 6 – Model based design:
Model based engineering of critical sistems.
Simulation and testing.
Code generation with Matlab Symulink.
